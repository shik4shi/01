const express = require('express');
const router = express.Router();
const events = require('../services/events');

/* GET slides listing. */
router.get('/', function(req, res, next) {
  
  global.nedb.find({ type: "SLIDES" }).sort({id: 1}).exec(function (err, doc) {
    
    if (err) {
      res.end(`ERROR::find ${err.message}`);
    }
    
    // We have existing slides so serve them
    if (doc && doc.length) {
      res.end(JSON.stringify(doc));
    } else {
      // Insert the slides
      let slides = [
        {
          id: 1,
          title: "Greetings from Miami",
          description: "Graffiti art by Tag",
          image_path: "1.jpg",
          type: "SLIDES"
        },
        {
          id: 2,
          title: "Blue Door",
          description: "Welcome to the blue door of the Shire",
          image_path: "2.jpg",
          type: "SLIDES"
        },
        {
          id: 3,
          title: "Red Camera",
          description: "Classic red camera on display",
          image_path: "3.jpg",
          type: "SLIDES"
        },
        {
          id: 4,
          title: "Singapore View",
          description: "The hands from Singapore",
          image_path: "4.jpg",
          type: "SLIDES"
        },
        {
          id: 5,
          title: "Airplane Close-up",
          description: "Perceptive view of an airplane",
          image_path: "5.jpg",
          type: "SLIDES"
        },
      ];
      
      let i;
      
      for (i = 0; i < slides.length; i++) {
        // Store our slide
        global.nedb.insert(slides[i], (err, doc) => {
          if (err) {
            res.end(`ERROR::insert ${err.message}`);
          }
        });
      }
      
      res.end(JSON.stringify(slides));
    }
  });
});

// This should be a post but is a GET Request for demo purposes
router.get('/create', function(req, res, next) {
  
  global.nedb.findOne({ type: "SLIDES" }).sort({id: -1}).exec(function (err, doc) {
    
    if (err) {
      res.end(`ERROR::find ${err.message}`);
    }
    
    // Continue to add our new slide up until 20
    const newId = doc ? doc.id + 1 : 1;
    
    if (newId <= 20) {
      const slide = {
        id: newId,
        title: `New Image #${newId}`,
        description: `New image description #${newId}`,
        image_path: `${newId}.jpg`,
        type: "SLIDES"
      };
  
      // Insert into the db
      global.nedb.insert(slide, (err, doc) => {
        if (err) {
          res.end(`ERROR::insert ${err.message}`);
        }
      });
  
      // Broadcast the event
      events.emitter().emit("BROADCAST_EVENT", Object.assign({
        event: "NEW_SLIDE_CREATED",
      }, slide));
  
      res.end(JSON.stringify(slide));
    } else {
      res.end("New slide limit reached");
    }
  });
  
});

module.exports = router;
