"use strict";
const nedbDataStore = require("nedb");
const path = require("path");

module.exports.init = () => {
  global.nedb = new nedbDataStore({
    filename: path.join("db", "nedb"),
    autoload: true
  });
  
};
