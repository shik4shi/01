const Events = require("events");

let emitter = new Events();

module.exports.emitter = () => {
  return emitter;
};
