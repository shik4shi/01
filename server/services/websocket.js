const WebSocket = require("ws");
const events = require("./events");
let wsPort, webSocketMap = {};
let scope = "WebSocket";

events.emitter().on("BROADCAST_EVENT", data => {
  exports.broadcastEvent({
    event: data.event,
    data: data
  });
});

exports.init = port => {
  wsPort = port;
  
  const wss = new WebSocket.Server({ port: port, perMessageDeflate: false });
  
  wss.on("error", error => {
    console.error(`${scope}::WS Error`, error);
  });
  
  wss.on("connection", function(ws, request, client) {
    const key = request.url.replace('/', '');
    webSocketMap[key] = {
      websocket: ws,
      address: `${request.headers.origin}/${key}`,
      identity: key,
      open: true
    };
  
    ws.on("close", function(code, reason) {
      console.info(scope, "Connection closed");
      webSocketMap[key].open = false;
    });
  });
};

exports.broadcastEvent = (event) => {
  scope = `${scope}::BroadcastEvent`;
  console.info(`${scope}::BroadcastEvent`, event.event);
  
  for (let key in webSocketMap) {
    if (webSocketMap.hasOwnProperty(key)) {
      console.debug(scope, `key: ${key}, mapExists: ${!!webSocketMap[key]}, event: ${event.event}`);
      
      try {
        console.info(scope, "forwarding websocket event", key, "@", webSocketMap[key].path);
        
        webSocketMap[key].websocket.send(JSON.stringify(event));
      } catch (error) {
        console.error(scope, "Error broadcasting event to Service @", key, "=>", error);
        delete webSocketMap[key];
      }
    }
  }
};
