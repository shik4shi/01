#React Slider

ReactJS the Slider component that will display a slide-show with previous and next buttons that 
wrap around infinitely. See `usage example` for slide array of object

####Usage example:
```
const slides = [
  {
    "id": 1,
    "title": "Greetings from Miami",
    "description": "Graffiti art by Tag",
    "image_path": "1.jpg",
    "type": "SLIDES"
  },
  {
    "id": 2,
    "title": "Blue Door",
    "description": "Welcome to the blue door of the Shire",
    "image_path": "2.jpg",
    "type": "SLIDES"
  }
];
               
<Slider slides={slides} />
```
