describe('End to end test of the slider', function () {
  
  it('test basic next action', function () {
    
    cy.viewport(320, 460);
    
    cy.visit('http://localhost:3000/');
    
    // Initial Slide (Greetings from Miami)
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
    // Next Slide (Blue Door)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Greetings from Miami");
    cy.get('.Slider-active > .Slider-text').contains("the Shire");
    cy.get('.Slider-next > span:first-child').contains("Red Camera");
    
    // Next Slide (Red Camera)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Blue Door");
    cy.get('.Slider-active > .Slider-text').contains("display");
    cy.get('.Slider-next > span:first-child').contains("Singapore View");
    
    // Next Slide (Singapore View)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Red Camera");
    cy.get('.Slider-active > .Slider-text').contains("The hands from Singapore");
    cy.get('.Slider-next > span:first-child').contains("Airplane Close-up");
    
    // Next Slide (Airplane Close-up)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
    
  });
  
  it('test basic previous action', function () {
    
    cy.viewport(1200, 900);
    
    // Initial Slide (Airplane Close-up)
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
    
    // Previous Slide (Singapore View)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Red Camera");
    cy.get('.Slider-active > .Slider-text').contains("The hands from Singapore");
    cy.get('.Slider-next > span:first-child').contains("Airplane Close-up");
    
    // Previous Slide (Red Camera)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Blue Door");
    cy.get('.Slider-active > .Slider-text').contains("display");
    cy.get('.Slider-next > span:first-child').contains("Singapore View");
    
    // Previous Slide (Blue Door)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Greetings from Miami");
    cy.get('.Slider-active > .Slider-text').contains("the Shire");
    cy.get('.Slider-next > span:first-child').contains("Red Camera");
    
    // Previous Slide (Greetings from Miami)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
  });
  
  it('test next wrap around', function () {
  
    cy.viewport(320, 460);
    
    cy.visit('http://localhost:3000/');
    
    // Initial Slide (Greetings from Miami)
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
    // Next Slide (Blue Door)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Greetings from Miami");
    cy.get('.Slider-active > .Slider-text').contains("the Shire");
    cy.get('.Slider-next > span:first-child').contains("Red Camera");
    
    // Next Slide (Red Camera)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Blue Door");
    cy.get('.Slider-active > .Slider-text').contains("display");
    cy.get('.Slider-next > span:first-child').contains("Singapore View");
    
    // Next Slide (Singapore View)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Red Camera");
    cy.get('.Slider-active > .Slider-text').contains("The hands from Singapore");
    cy.get('.Slider-next > span:first-child').contains("Airplane Close-up");
    
    // Next Slide (Airplane Close-up)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
    
    // Next Slide wrap around (Greetings from Miami)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
    // Previous Slide wrap around (Airplane Close-up)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
  
    // Next Slide wrap around (Greetings from Miami)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
  
  });
  
  it('test previous wrap around', function () {
    
    cy.viewport(1200, 900);
    
    cy.visit('http://localhost:3000/');
    
    // Initial Slide (Greetings from Miami)
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
  
    // Previous Slide wrap around (Airplane Close-up)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
  
    // Previous Slide (Singapore View)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Red Camera");
    cy.get('.Slider-active > .Slider-text').contains("The hands from Singapore");
    cy.get('.Slider-next > span:first-child').contains("Airplane Close-up");
  
    // Previous Slide (Red Camera)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Blue Door");
    cy.get('.Slider-active > .Slider-text').contains("display");
    cy.get('.Slider-next > span:first-child').contains("Singapore View");
    
    // Previous Slide (Blue Door)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Greetings from Miami");
    cy.get('.Slider-active > .Slider-text').contains("the Shire");
    cy.get('.Slider-next > span:first-child').contains("Red Camera");
  
    // Previous Slide (Greetings from Miami)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
  });
  
  it('test previous and next wrap around', function () {
    
    cy.viewport(600, 900);
    
    cy.visit('http://localhost:3000/');
    
    // Initial Slide (Greetings from Miami)
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
    // Next Slide (Blue Door)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Greetings from Miami");
    cy.get('.Slider-active > .Slider-text').contains("the Shire");
    cy.get('.Slider-next > span:first-child').contains("Red Camera");
  
    // Previous Slide (Greetings from Miami)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
  
    // Previous Slide wrap around (Airplane Close-up)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
  
    // Previous Slide (Singapore View)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-prev-block > .Slider-prev').click();
    cy.get('.Slider-prev > span:last-child').contains("Red Camera");
    cy.get('.Slider-active > .Slider-text').contains("The hands from Singapore");
    cy.get('.Slider-next > span:first-child').contains("Airplane Close-up");
    
    // Next Slide (Airplane Close-up)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Singapore View");
    cy.get('.Slider-active > .Slider-text').contains("airplane");
    cy.get('.Slider-next > span:first-child').contains("Greetings from Miami");
    
    // Next Slide wrap around (Greetings from Miami)
    cy.get('.App-body > .Slider-container > .Slider-container-inner > .Slider-next-block > .Slider-next').click();
    cy.get('.Slider-prev > span:last-child').contains("Airplane Close-up");
    cy.get('.Slider-active > .Slider-text').contains("Graffiti art by Tag");
    cy.get('.Slider-next > span:first-child').contains("Blue Door");
    
  });
  
});
