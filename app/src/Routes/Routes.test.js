import React from 'react'
import { getSlides } from './Routes';

// Ensure our API data is still returning the correct data
describe('Action Dispatcher Test', () => {

  it('check action on dispatching ', async () => {

    const log = await getSlides();

    expect(log).toBeDefined();

    expect(log).toBeInstanceOf(Array);

    let i;
    for (i = 0; i < log.length; i++) {
      expect(log[i].id).toBeDefined();
    }
  });

});
