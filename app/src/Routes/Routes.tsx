/**
 * @name Routes
 * @description All API routing is handled here
 */

import Config from "../Config";

export async function getSlides () {
  const response = await fetch(`${Config.apiUrl}/slides`, {
    headers: {
      'Content-Type': 'application/json',
    }
  });

  return await response.json();

}
