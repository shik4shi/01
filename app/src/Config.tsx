/**
 * @name Config
 * @description All configuration settings, api keys and api urls are added here
 */

export default {
  "apiUrl": "http://localhost:3333",
  "wsUrl": "ws://127.0.0.1:8008"
}
