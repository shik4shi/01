/**
 * @name Components: Slider
 * @description accepts an array of slides property and displays them in a slide-show with a description and
 * previous and next buttons
 */

import React from "react";
import './Slider.css';

import config from "../../Config";
import ISlides from "../../Interfaces/Interface.slides";

interface SliderProps {
  slides: Array<ISlides>
}

/**
 * Slider Component Class
 */
export class Slider extends React.Component<SliderProps, any> {

  constructor (props: SliderProps) {
    super(props);

    this.state = {
      slideIndex: 0
    }
  }

  /*
   * Action for the slider: "prev" to show the previous slide, "next" to show the next slide.
   *
   * If we are viewing the first image in the slide the "prev" button will load the last image.
   * If we are viewing the last image in the slide the "next" button will load the first image.
   */
  toggleSlide = (action: string) => {
    let slideIndex = this.state.slideIndex;

    switch (action) {
      case "prev":
        slideIndex = slideIndex === 0 ? this.props.slides.length - 1 : slideIndex - 1;
        break;
      case "next":
        slideIndex = slideIndex === this.props.slides.length - 1 ? 0 : slideIndex + 1;
        break;
    }

    this.setState({slideIndex: slideIndex});
  };

  render () {

    return (
      <div className="Slider-container">

        <div className="Slider-container-inner">

          {
            this.props.slides.map((slide: ISlides, index: number) => (
              <div className={'Slider-slide fade' + (index === this.state.slideIndex ? ' Slider-active' : '')} key={index}>
                <img src={`${config.apiUrl}/images/${slide.image_path}`} alt={slide.title} />
                <div className="Slider-text">{slide.description}</div>
              </div>
              )
            )
          }

          <div className="Slider-prev-block" onClick={() => this.toggleSlide('prev')}>
            <div className="Slider-prev"><span>&#10094;</span> <span>{this.props.slides.length ? (this.state.slideIndex === 0 ? this.props.slides[this.props.slides.length - 1] && this.props.slides[this.props.slides.length - 1].title : this.props.slides[this.state.slideIndex - 1].title) : ''}</span></div>
          </div>

          <div className="Slider-next-block" onClick={() => this.toggleSlide('next')}>
            <div className="Slider-next"><span>{this.props.slides.length ? (this.state.slideIndex === this.props.slides.length - 1 ? this.props.slides[0] && this.props.slides[0].title : this.props.slides[this.state.slideIndex + 1] && this.props.slides[this.state.slideIndex + 1].title) : ''}</span> <span>&#10095;</span></div>
          </div>

        </div>

      </div>
    );

  }
}

export default Slider;
