import React from 'react';
import TestRenderer from 'react-test-renderer';
import Slider from "./Slider";
import { getSlides } from "../../Routes/Routes";

describe('Slider Render Test', () => {
  
  it('Renders as expected', async () => {
  
    let slides = await getSlides();
    
    const renderedValue = TestRenderer.create(<Slider slides={slides} />);
  
    expect(renderedValue).toMatchSnapshot();
  });
  
});
