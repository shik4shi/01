/**
 * @name Interface: Slides
 * @description Interface for a Slide entry
 */

export default interface ISlides {
  id: number;
  title?: string;
  description?: string;
  image_path?: string;
}
