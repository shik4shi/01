/**
 * @name Interface: WebSocket data
 * @description Interface for receiving WebSocket messages
 */

export default interface IWebSocketData {
  data: string;
}
