/**
 * @name App
 * @description Example web app displaying the React Slider Component with Node.js server serving the slides
 */

import React from 'react';
import './App.css';
import { getSlides } from "../Routes/Routes";
import {Slider} from "../Components/Slider/Slider";
import ISlides from "../Interfaces/Interface.slides";
import IWebSocketData from "../Interfaces/Interface.webSocket.data";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import Config from '../Config';

// Closure to generate an unique key
const uniqueKey = () => {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4();
};
const client = new W3CWebSocket(`${Config.wsUrl}/${uniqueKey()}`);

export default class App extends React.Component<{}, {slides: Array<ISlides>}> {

  constructor (props: object) {
    super(props);

    this.state = {
      slides: []
    }
  }

  async componentDidMount () {
    const slides = await getSlides();

    client.onopen = () => {
      console.log('WebSocket Client Connected');
    };
    client.onmessage = (message: IWebSocketData) => {
      const data = JSON.parse(message.data);

      // WebSocket Handler
      switch(data.event) {
        case "NEW_SLIDE_CREATED":
          slides.push(data.data);
          this.setState({slides: slides});
          break;
        default:
          break;
      }
    };

    // Update our state with the list of slides retrieved from the server
    this.setState({slides: slides});
  }

  render () {
    return (
      <div className="App">
        <header className="App-header">
          <h1>React Slider</h1>
        </header>
        <div className="App-body">
          <Slider slides={this.state.slides}/>
        </div>
      </div>
    );
  }
}
