#BNRY Setup Guide

#Server

```
cd [root]/server
yarn
yarn start
```

#App

```
cd [root]/app
yarn
yarn start
```

####Creating new slides on the fly

Go to `http://localhost:3333/slides/create`

This will create 1 new slide. Hit the same address to add another slide. It will create
a maximum 15 more slides
